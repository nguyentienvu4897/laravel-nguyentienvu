<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PositionController;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::resource('blog', [BlogController::class]);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categories', [CategoryController::class, 'getAll']);
Route::get('categories/{category}', [CategoryController::class, 'show']);
Route::post('categories', [CategoryController::class, 'store']);
Route::put('categories/{category}', [CategoryController::class, 'update']);
Route::delete('categories/{category}', [CategoryController::class, 'delete']);

Route::get('blogs', [BlogController::class, 'getAll']);
Route::get('blogs/{blog}', [BlogController::class, 'getId']);
Route::get('blogs/search/{blog}', [BlogController::class, 'searchName']);
Route::post('blogs', [BlogController::class, 'create']);
Route::put('blogs/{blog}', [BlogController::class, 'update']);
Route::delete('blogs/{blog}', [BlogController::class, 'delete']);

Route::get('positions', [PositionController::class, 'getAll']);

