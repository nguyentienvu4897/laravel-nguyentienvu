<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Http\Request;

class BlogRepository extends BaseRepository
{
    /**
     * Search record in the blog table by name
     * 
     * @param $name
     * @return array
     */
    public function search($name)
    {
        $blogs = $this->model
        ->where('title', 'like', '%' . $name . '%')
        ->paginate(5);
        return $blogs;
    }
}