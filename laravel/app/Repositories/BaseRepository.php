<?php
namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all records from the table
     * 
     * @return array
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Create new records to the table
     * 
     * @param $data
     * @return array
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * Update record to the table by id
     * 
     * @param $data
     * @param $id
     * @return boolean
     */
    public function update($data, $id)
    {
        $record = $this->model->find($id);
        return $record->update($data);
    }

    /**
     * Delete record in the table by id
     * 
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Show record in the table by id
     * 
     * @param $id
     * @return array
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Get model 
     * 
     * @return void
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set model
     * 
     * @param $model
     * @return void
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }
}