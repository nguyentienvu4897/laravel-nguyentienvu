<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Psy\TabCompletion\Matcher\FunctionsMatcher;

class Blog extends Model
{
    use HasFactory;

    protected $table = 'blogs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['title', 'desc', 'detail', 'category_id', 'public', 'position_id'];

    public function category()
    {
        return $this->belongsTo('categories', 'category_id');
    }

    public function positions()
    {
        return $this->belongsToMany('positions', 'blog_positions');
    }
}
