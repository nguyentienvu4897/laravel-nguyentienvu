<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Http\Resources\BlogResource;
use App\Models\Blog;
use Illuminate\Http\Request;
use App\Repositories\BlogRepository;

class BlogController extends Controller
{
    protected $model;

    /**
     * Declare constructor with Blog model
     * 
     * @param Blog
     * @param $blog
     * @return object
     */
    public function __construct(Blog $blog)
    {
        $this->model = new BlogRepository($blog);
    }

    /**
     * Create record to the Blog table
     * 
     * @param BlogRequest
     * @param $request
     * @return array
     */
    public function create(Request $request)
    {
        $blog = $this->model->create($request->all());
        return response()->json( new BlogResource($blog));
    }

    /**
     * Get all records from the Blog table
     * 
     * @return array
     */
    public function getAll()
    {
        $blogs = $this->model->getAll();
            return response()->json( BlogResource::collection($blogs)
        );
    }

    /**
     * Search record from the blog table by name
     * 
     * @param $name
     * @return array
     */
    public function searchName($name)
    {
        $blog = $this->model->search($name);
        if(isset($blog)){
            return response()->json(BlogResource::collection($blog));
        }
    }

    public function getId($id)
    {
        $getBlogById = $this->model->find($id);
        return response()->json(new BlogResource($getBlogById));
    }

    /**
     * Update record to the blog table by id
     * 
     * @param BlogRequest
     * @param $request
     * @param $id
     * @return boolean
     */
    public function update(Request $request, $id)
    {
        $this->model->update($request->input(), $id);
    }

    /**
     * Remove record to the blog table by id
     * 
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        $this->model->delete($id);
    }
}
