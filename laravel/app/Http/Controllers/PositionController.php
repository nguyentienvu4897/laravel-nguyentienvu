<?php

namespace App\Http\Controllers;

use App\Http\Resources\PositionResource;
use App\Models\Position;
use App\Repositories\PositionRepository;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    protected $model;

    /**
     * Declare constructor with Category model
     * 
     * @param Category
     * @param $category
     * @return object
     */
    public function __construct(Position $position)
    {
        $this->model = new PositionRepository($position);
    }

    /**
     * Get all records from Category table
     * 
     * @return array
     */
    public function getAll()
    {
        $positions = $this->model->getAll();
        return response()->json( PositionResource::collection($positions));
    }
}
