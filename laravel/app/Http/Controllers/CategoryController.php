<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $model;

    /**
     * Declare constructor with Category model
     * 
     * @param Category
     * @param $category
     * @return object
     */
    public function __construct(Category $category)
    {
        $this->model = new CategoryRepository($category);
    }

    /**
     * Get all records from Category table
     * 
     * @return array
     */
    public function getAll()
    {
        $categories = $this->model->getAll();
        return response()->json( CategoryResource::collection($categories));
    }

    /**
     * Get record from Category table by id
     * 
     * @param $id
     * @return array
     */
    public function show($id)
    {
        $category = $this->model->find($id);
        return response()->json([
            'data' => new CategoryResource($category),
            'status' => true
        ]);
    }

    /**
     * Create new record to the Category table
     * 
     * @param CategoryRequest
     * @param $request
     * @return array
     */
    public function store(Request $request)
    {
        $categories = $this->model->create($request->all());
        return response()->json([
            'data'=> new CategoryResource($categories),
            'status'=>true
        ]);
    }

    /**
     * Update record to the Category table by id
     * 
     * @param Request
     * @param $request
     * @param $id
     * @return boolean
     */
    public function update(Request $request, $id)
    {
        $category = $this->model->update($request->all(), $id);
        return response()->json([
            'data'=> new CategoryResource($category),
            'status'=>true
        ]);
    }

    /**
     * Remove record to the Category table by id
     * 
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        return $this->model->delete($id);
    }
}
