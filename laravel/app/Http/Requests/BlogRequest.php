<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'desc' => 'required',
            'detail' => 'required',
            'category_id' => 'required',
            'position' => 'required',
            'public' => 'required',
        ];
    }

    /**Get the validation messages that apply to the request
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'name_blog.required'=> 'Please type name!',
            'description_blog.required' => 'Please type description!',
            'author_blog.required' => 'Please type description!'
        ];
    }
}
